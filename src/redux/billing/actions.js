export const BILLING_FETCH_TRIPS_REQUEST =  '@billing/FETCH_TRIPS_REQUEST'
export const BILLING_FETCH_TRIPS_SUCCESS =  '@billing/FETCH_TRIPS_SUCCESS'
export const BILLING_FETCH_TRIPS_ERROR =  '@billing/FETCH_TRIPS_ERROR'
export const BILLING_DOWNLOAD_INVOICE_REQUEST =  '@billing/BILLING_DOWNLOAD_INVOICE_REQUEST'
export const BILLING_DOWNLOAD_INVOICE_SUCCESS =  '@billing/BILLING_DOWNLOAD_INVOICE_SUCCESS'
export const BILLING_DOWNLOAD_INVOICE_ERROR =  '@billing/BILLING_DOWNLOAD_INVOICE_ERROR'

export const downloadInvoiceRequest = (invoiceId) => {
    return ({
       type: BILLING_DOWNLOAD_INVOICE_REQUEST,
       payload: { invoiceId }
    });
}

export const fetchMyTripsRequest = () => {
    return {
       type: BILLING_FETCH_TRIPS_REQUEST
    }
};