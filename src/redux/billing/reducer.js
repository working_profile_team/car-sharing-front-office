import {
    BILLING_FETCH_TRIPS_SUCCESS,
    BILLING_DOWNLOAD_INVOICE_ERROR,
    BILLING_FETCH_TRIPS_REQUEST
} from "./actions";

export const initState = {
    isFetching: false,
    error: '',
    data: []
}

export default function billingReducer(state = initState, action = {}) {
    switch(action.type) {

        case BILLING_FETCH_TRIPS_REQUEST :
            return {
                ...state,
                isFetching: true,
                error: '',
                data: []
            }

        case BILLING_FETCH_TRIPS_SUCCESS :
            return {
                ...state,
                isFetching: false,
                error: '',
                data: {...action.payload}
            }

        case BILLING_DOWNLOAD_INVOICE_ERROR :
            return {
                ...state,
                isFetching: false,
                error: {...action.payload},
                data: []
            }

        default:  return { ...state }
    }
}