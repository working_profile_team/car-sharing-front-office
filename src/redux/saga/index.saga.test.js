import rootSaga from './index'

describe('root saga', () => {
    it('spawns two sagas', () => {
        const rootSagaGenerator = rootSaga()
        expect(typeof rootSagaGenerator.next().value.FORK).toEqual('object')
        expect(typeof rootSagaGenerator.next().value.FORK).toEqual('object')
    })
})