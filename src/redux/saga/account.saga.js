import {all, call, fork, put, select, take} from 'redux-saga/effects'
import {
    AUTH_ANONYMOUS_FETCH_TOKEN,
    AUTH_ANONYMOUS_FETCH_TOKEN_ERROR,
    AUTH_ANONYMOUS_FETCH_TOKEN_REQUEST,
    AUTH_ANONYMOUS_FETCH_TOKEN_SUCCESS,
    SAGA_FETCH_USER_FAILURE,
    SAGA_FETCH_USER_REQUEST,
    SAGA_FETCH_USER_SUCCESS,
    SAGA_UPDATE_USER_FAILURE,
    SAGA_UPDATE_USER_REQUEST,
    SAGA_UPDATE_USER_SUCCESS,
    SEND_LOST_PASSWORD_EMAIL_FULFILLED,
    SEND_LOST_PASSWORD_EMAIL_REJECTED,
    SEND_LOST_PASSWORD_EMAIL_REQUEST,
    SEND_LOST_PASSWORD_FORM,
    SET_ANONYMOUS_TOKEN,
} from '../account/accountAction'
import {
    SET_LANGUAGE
} from '../language/languageAction'

import {
    getUser,
    getUserPaymentDetails
} from './helpers/account.saga'

import {LOGOUT} from '../login/loginAction'


export function* fetchUser(apiClients) { // if no anon token in the store, get a new one
    while (true) {

        yield take(SAGA_FETCH_USER_REQUEST);
        try {
            const user = yield call(getUser, apiClients);
            const userPaymentDetails = yield call(getUserPaymentDetails, apiClients, user.entityId);
            if (user.locale){
                yield put({type: SET_LANGUAGE, language: user.locale})
            }
            yield put({type: SAGA_FETCH_USER_SUCCESS, payload: {user, userPaymentDetails}})
        } catch (error) {
            if(error.code==="401"){
                yield put({type: LOGOUT, payload: error})
            }else{
                yield put({type: SAGA_FETCH_USER_FAILURE, payload: error})
            }

        }
    }
}

export function* updateUserSaga(apiClients) {
    while (true) {
        const { userData } = yield take(SAGA_UPDATE_USER_REQUEST);
        try {
            const accessToken =  yield select(accessTokenSelector);
            yield call(apiClients.user.updateUser, accessToken, userData);
            yield put({type: SAGA_UPDATE_USER_SUCCESS});
            yield put({type: SAGA_FETCH_USER_REQUEST})
        } catch (error) {
            yield put({type: SAGA_UPDATE_USER_FAILURE, payload: error})
        }
    }
}

/**
 * Create an account management Saga
 *
 * @param {object} apiClients The common FrontOffice Saga API Object
 * @returns {generator} account saga generator object
 */
export default function* accountSaga(apiClients) {
    yield all([
        fork(sendLostPasswordSaga, apiClients),
        fork(requestAnonymousToken, apiClients),
        fork(getAnonymousToken),
        fork(fetchUser, apiClients),
        fork(updateUserSaga, apiClients)
    ]);
}