import {fork, take, put, select, call} from 'redux-saga/effects'
import {
    BILLING_FETCH_TRIPS_REQUEST,
    BILLING_FETCH_TRIPS_SUCCESS,
    BILLING_FETCH_TRIPS_ERROR,
    BILLING_DOWNLOAD_INVOICE_REQUEST,
    BILLING_DOWNLOAD_INVOICE_SUCCESS
} from "../billing/actions";

import {downloadBlobPDFData} from "../../helpers/common";

import {
    fetchMyTrips
} from './helpers/billing.saga'

const accessTokenSelector = state => state.loginReducer.auth.access_token;

const sagaFetchMyTrips = function*(apiClients) {
    while(true){
        yield take(BILLING_FETCH_TRIPS_REQUEST);
        try {
            const trips = yield call(fetchMyTrips, apiClients);
            yield put({type: BILLING_FETCH_TRIPS_SUCCESS, payload: trips.Billing})
        } catch(error) {
            yield put({type: BILLING_FETCH_TRIPS_ERROR, payload: error})
        }
    }
}

export const downloadInvoice = function*(api) {
    while(true) {
        const { payload } = yield take(BILLING_DOWNLOAD_INVOICE_REQUEST)
        const token = yield select(accessTokenSelector)
        const {success} = yield call(api.user.downloadInvoice, token, payload.invoiceId)
        if (success) {
            downloadBlobPDFData(success, 'invoice');
            yield put({type: BILLING_DOWNLOAD_INVOICE_SUCCESS})
        }
    }
}

export default function* billingSaga(api) {
    yield fork(sagaFetchMyTrips, api)
    yield fork(downloadInvoice, api)
}