import {onLoadLanguage, selectedLanguageSelector} from './language.saga'

const mockState = {
    languageReducer: {
        language: 'nyan'
    }
}

describe('language saga', () => {
    it('selects a language from the state object', () => {
        expect(selectedLanguageSelector(mockState)).toEqual('nyan')
    })

    it('puts the default language back into the store', () => {
        const mockApiClients = {
            apiConf: {
                forceLang: 'en_EN'
            }
        }
        const onLoadLanguageGenerator = onLoadLanguage(mockApiClients);
        expect(onLoadLanguageGenerator.next().value.SELECT).toBeDefined();
        const putValue = onLoadLanguageGenerator.next().value;
        expect(putValue.PUT).toBeDefined();
        expect(putValue.PUT.action.language).toEqual('en_EN');
    })

    it('puts the selected language back into the store', () => {
        const onLoadLanguageGenerator = onLoadLanguage();
        expect(onLoadLanguageGenerator.next().value.SELECT).toBeDefined();
        const putValue = onLoadLanguageGenerator.next('nyan').value;
        expect(putValue.PUT).toBeDefined();
        expect(putValue.PUT.action.language).toEqual('nyan');
    })
})