import {call, select} from 'redux-saga/effects'

import {accessTokenSelector} from '../../../selectors/login';

export const fetchMyTrips = function* (apiClients) {
    let accessToken = yield select(accessTokenSelector);
    return yield call(apiClients.user.fetchMyTrips, accessToken);
};