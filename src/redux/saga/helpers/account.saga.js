import {call, select} from 'redux-saga/effects'
import {accessTokenSelector} from '../../../selectors/login';

export const getUser = function* (apiClients) {
    const accessToken = yield select(accessTokenSelector);
    return yield call(apiClients.user.getUser, accessToken);
};

export const getUserPaymentDetails = function* (apiClients, entityId) {
    const accessToken = yield select(accessTokenSelector);
    return yield call(apiClients.user.getPaymentDetails, accessToken, entityId);
};
