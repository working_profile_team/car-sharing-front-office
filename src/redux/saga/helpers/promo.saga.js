import {call, take, select} from 'redux-saga/effects'

import {
    accessTokenSelector
} from '../../../selectors/login';

import {
    userSelector
} from '../../../selectors/account';

import {
    SAGA_FETCH_USER_SUCCESS
} from '../../account/accountAction';

export const fetchMyPromoCodes = function* (apiClients) {
    let accessToken = yield select(accessTokenSelector);
    let user = yield select(userSelector);
    if (!user.entityId){
        yield take(SAGA_FETCH_USER_SUCCESS);
        user = yield select(userSelector);
    }
    return yield call(apiClients.user.fetchMyPromoCodes, accessToken, user.entityId);
};