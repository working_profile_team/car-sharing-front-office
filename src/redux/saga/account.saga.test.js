import accountSaga, {sendLostPasswordSaga} from './account.saga'
import {
    AUTH_ANONYMOUS_FETCH_TOKEN, AUTH_ANONYMOUS_FETCH_TOKEN_SUCCESS,
    SEND_LOST_PASSWORD_EMAIL_REQUEST
} from "../account/accountAction";

describe('sendLostPasswordSaga (accountSaga) saga', () => {
    it('waits for a SEND_LOST_PASSWORD_EMAIL_REQUEST', () => {
        const mockApiObj = {
            user: {
                sendLostPasswordEmail: () => ({success: true})
            }
        }
        const sendLostPasswordSagaGen = sendLostPasswordSaga(mockApiObj)
        expect(sendLostPasswordSagaGen.next().value.TAKE.pattern).toEqual(SEND_LOST_PASSWORD_EMAIL_REQUEST)
    })
    it('puts a send lost password email fulfill on success call', () => {
        const mockApiObj = {
            user: {
                sendLostPasswordEmail: () => ({success: {anonymousToken: 'helloWally'}})
            }
        }
        const sendLostPasswordSagaGen = sendLostPasswordSaga(mockApiObj)
        expect(sendLostPasswordSagaGen.next().value.TAKE.pattern).toEqual(SEND_LOST_PASSWORD_EMAIL_REQUEST)
        expect(typeof sendLostPasswordSagaGen.next().value.SELECT.selector).toEqual('function')
        expect(sendLostPasswordSagaGen.next('recovery@email.com').value.PUT.action.type).toEqual(AUTH_ANONYMOUS_FETCH_TOKEN)
        expect(sendLostPasswordSagaGen.next().value.TAKE.pattern).toEqual(AUTH_ANONYMOUS_FETCH_TOKEN_SUCCESS)
        expect(typeof sendLostPasswordSagaGen.next().value.SELECT.selector).toEqual('function')
        expect(sendLostPasswordSagaGen.next({anonymousToken: 'helloWally'}).value.CALL.args[0]).toEqual('helloWally')
    })
})