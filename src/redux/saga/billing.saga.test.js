import billingSaga, {downloadInvoice} from './billing.saga'
import {
    BILLING_DOWNLOAD_INVOICE_REQUEST
} from "../billing/actions";

describe('billing saga ', () => {
    it('forks two sagas', () => {
        const billingSagaGenerator = billingSaga()
        expect(typeof billingSagaGenerator.next().value.FORK).toEqual('object')
        expect(typeof billingSagaGenerator.next().value.FORK).toEqual('object')
        expect(billingSagaGenerator.next().done).toEqual(true)
    })
})

describe('sendLostPasswordSaga (billingSaga) saga', () => {
    it('waits for a BILLING_DOWNLOAD_INVOICE_REQUEST', () => {
        const mockApiObj = {
            user: {
                downloadInvoice: () => ({success: 'blob'})
            }
        }
        const downloadInvoiceGen = downloadInvoice(mockApiObj)
        expect(downloadInvoiceGen.next().value.TAKE.pattern).toEqual(BILLING_DOWNLOAD_INVOICE_REQUEST)
    })
    it('calls the download API with an invoiceId', () => {
        const mockApiObj = {
            user: {
                downloadInvoice: () => ({success: 'blob'})
            }
        }
        const downloadInvoiceGen = downloadInvoice(mockApiObj)
        expect(downloadInvoiceGen.next().value.TAKE.pattern).toEqual(BILLING_DOWNLOAD_INVOICE_REQUEST)
        expect(typeof downloadInvoiceGen.next({payload: {invoiceId: '123'}}).value.SELECT.selector).toEqual('function')
        expect(downloadInvoiceGen.next().value.CALL.args[1]).toEqual('123')
    })
})