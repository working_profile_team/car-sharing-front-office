import {spawn} from "redux-saga/effects";
import configSaga from './config.saga'
import languageSaga from './language.saga'
import authSaga from './auth.saga'
import accountSaga from './account.saga'
import billingSaga from './billing.saga'
import promoSaga from './promo.saga'
import UserClient from '../../api/UserClient'
import VehicleClient from "../../api/VehicleClient";
import AnonymousClient from "../../api/AnonymousClient";

const clientName = process.env.REACT_APP_CLIENT_NAME || 'vulog';
const apiConf = require('../../resources/api/' + clientName + '.json');

const api = {
    user: new UserClient(apiConf),
    vehicle: new VehicleClient(apiConf),
    anonymous: new AnonymousClient(apiConf),
    apiConf
}
export default function* rootSaga() {
    yield spawn(configSaga)
    yield spawn(languageSaga, api)
    yield spawn(authSaga, api)
    yield spawn(accountSaga, api)
    yield spawn(billingSaga, api)
    yield spawn(promoSaga, api)
}