import {fork, put, select} from 'redux-saga/effects'
import {setlanguage} from "../language/languageAction";

export const selectedLanguageSelector = state => state.languageReducer.language;

export function* onLoadLanguage(apiClients) {
    const persistedLanguage =  yield select(selectedLanguageSelector);
    const loadLanguage = (persistedLanguage) ? persistedLanguage : apiClients.apiConf.forceLang;
    yield put(setlanguage(loadLanguage))
}



export default function* languageSaga(apiClients) {
    yield fork(onLoadLanguage, apiClients)
}