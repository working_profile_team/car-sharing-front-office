import {
    GET_USER_PENDING,
    GET_USER_REJECTED,
    GET_USER_FULFILLED,

    UPDATE_USER_PENDING,
    UPDATE_USER_REJECTED,
    UPDATE_USER_FULFILLED,

    RESET_ACCOUNT_REDUCER,
    RESET_USER_UPDATE,

    CHANGE_PASSWORD_PENDING,
    CHANGE_PASSWORD_FULFILLED,
    CHANGE_PASSWORD_REJECTED,

    SEND_LOST_PASSWORD_EMAIL_PENDING,
    SEND_LOST_PASSWORD_EMAIL_FULFILLED,
    SEND_LOST_PASSWORD_EMAIL_REJECTED,

    RESET_LOST_PASSWORD_PENDING,
    RESET_LOST_PASSWORD_FULFILLED,
    RESET_LOST_PASSWORD_REJECTED,
    SET_ANONYMOUS_TOKEN,

    FETCH_USER_PAYMENT_DETAILS_REQUEST,
    FETCH_USER_PAYMENT_DETAILS_SUCCESS,
    UPDATE_PAYMENT_DATA_REJECTED,

    SAGA_FETCH_USER_SUCCESS,
    SAGA_FETCH_USER_FAILURE,
} from './accountAction';

let initState = {
    fetching: false,
    fetched: false,
    user: {},
    paymentDetails: null,
    error: null,
    updateUserFetched: false,
    reply: null,
    //should be true if success and false if not.
    // only false is being used for now
    updatePaymentData: null,
};


const accountReducer = (state = initState, action) => {

    switch (action.type) {
        case RESET_ACCOUNT_REDUCER:
            return Object.assign({}, initState);

        case GET_USER_PENDING:
            return Object.assign({}, state, {
                fetching: true,
                error: null,
                fetched: false
            });

        case GET_USER_FULFILLED:
            return Object.assign({}, state, {
                fetching: false,
                error: null,
                user: action.payload,
                fetched: true,
            });

        case GET_USER_REJECTED:
            return Object.assign({}, state, {
                fetching: false,
                error: JSON.parse(action.payload),
                fetched: false
            });


        case UPDATE_USER_PENDING:
            return {
                ...state,
                fetching: true,
                error: null,
                fetched: false,
                updateUserFetched: false,
            };

        case UPDATE_USER_FULFILLED:
            return {
                ...state,
                user: action.payload,
                fetching: false,
                error: null,
                fetched: true,
                updateUserFetched: true,
            };


        case UPDATE_USER_REJECTED:
            return {
                ...state,
                fetching: false,
                error: JSON.parse(action.payload),
                user: {},
                fetched: true,
                updateUserFetched: true,
            };


        case RESET_USER_UPDATE:
            return Object.assign({}, state, {
                updateUserFetched: false,
            })


        case CHANGE_PASSWORD_PENDING:
            return Object.assign({}, state, {
                fetching: true,
                error: null,
                fetched: false,
                updateUserFetched: false,
            });

        case CHANGE_PASSWORD_REJECTED:
            return Object.assign({}, state, {
                fetching: false,
                error: JSON.parse(action.payload),
                fetched: false,
                updateUserFetched: true,
            });

        case CHANGE_PASSWORD_FULFILLED:
            return Object.assign({}, state, {
                fetching: false,
                error: null,
                fetched: true,
                updateUserFetched: true,
            });

        case SEND_LOST_PASSWORD_EMAIL_PENDING:
            return Object.assign({}, state, {
                fetching: true,
                error: null,
                fetched: false,
                reply: null
            });

        case SEND_LOST_PASSWORD_EMAIL_FULFILLED:
            return Object.assign({}, state, {
                fetching: false,
                reply: action.payload ? JSON.parse(action.payload) : null, /* TODO REFACTOR THIS*/
                fetched: true,
                error: null,
            });

        case SEND_LOST_PASSWORD_EMAIL_REJECTED:
            return Object.assign({}, state, {
                fetching: false,
                error: action.payload,
                fetched: true,
                reply: null
            });

        case RESET_LOST_PASSWORD_PENDING:
            return Object.assign({}, state, {
                fetching: true,
                error: null,
                fetched: false,
                reply: null
            });

        case RESET_LOST_PASSWORD_FULFILLED:
            return Object.assign({}, state, {
                fetching: false,
                reply: JSON.parse(action.payload),
                fetched: true,
            });

        case RESET_LOST_PASSWORD_REJECTED:
            return Object.assign({}, state, {
                fetching: false,
                error: JSON.parse(action.payload),
                fetched: true,
                reply: null
            });
        case SET_ANONYMOUS_TOKEN:
            return {
                ...state,
                anonymousToken: action.payload
            }

        case FETCH_USER_PAYMENT_DETAILS_REQUEST:
            return {
                ...state,
                fetching: true,
                error: null,
                fetched: false,
                reply: null,
                paymentDetails: null
            };

        case FETCH_USER_PAYMENT_DETAILS_SUCCESS:
            return {
                ...state,
                fetching: false,
                error: null,
                fetched: true,
                reply: null,
                paymentDetails: action.payload
            };
        case UPDATE_PAYMENT_DATA_REJECTED:
            return {
                ...state,
                updatePaymentData: false
            }

        case SAGA_FETCH_USER_SUCCESS:
            return {
                ...state,
                fetching: false,
                error: null,
                user: action.payload.user,
                paymentDetails: action.payload.userPaymentDetails,
                fetched: true,
            };

        case SAGA_FETCH_USER_FAILURE:
            return {
                ...state,
                fetching: false,
                error: JSON.parse(action.payload),
                fetched: false,
            };

        default: return state;
    }
}

export default accountReducer;
