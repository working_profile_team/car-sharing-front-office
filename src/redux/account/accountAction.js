export const RESET_ACCOUNT_REDUCER = "@account/RESET_ACCOUNT_REDUCER";

export const AUTH_ANONYMOUS_FETCH_TOKEN = '@account/AUTH_ANONYMOUS_FETCH_TOKEN'
export const AUTH_ANONYMOUS_FETCH_TOKEN_REQUEST = '@account/AUTH_ANONYMOUS_FETCH_TOKEN_REQUEST'
export const AUTH_ANONYMOUS_FETCH_TOKEN_SUCCESS = '@account/AUTH_ANONYMOUS_FETCH_TOKEN_SUCCESS'
export const AUTH_ANONYMOUS_FETCH_TOKEN_ERROR = '@account/AUTH_ANONYMOUS_FETCH_TOKEN_ERROR'

export const UPDATE_USER = "@account/UPDATE_USER";
export const UPDATE_USER_PENDING = "@account/UPDATE_USER_PENDING";
export const UPDATE_USER_FULFILLED = "@account/UPDATE_USER_FULFILLED";
export const UPDATE_USER_REJECTED = "@account/UPDATE_USER_REJECTED";

export const GET_USER = "@account/GET_USER";
export const GET_USER_PENDING = "@account/GET_USER_PENDING"
export const GET_USER_FULFILLED = "@account/GET_USER_FULFILLED"
export const GET_USER_REJECTED = "@account/GET_USER_REJECTED"

export const RESET_USER_UPDATE = "@account/RESET_USER_UPDATE"

export const CHANGE_PASSWORD = "@account/CHANGE_PASSWORD";
export const CHANGE_PASSWORD_PENDING = "@account/CHANGE_PASSWORD_PENDING"
export const CHANGE_PASSWORD_FULFILLED = "@account/CHANGE_PASSWORD_FULFILLED"
export const CHANGE_PASSWORD_REJECTED = "@account/CHANGE_PASSWORD_REJECTED"
export const SET_ANONYMOUS_TOKEN = "@account/SET_ANON_TOKEN"
export const SEND_LOST_PASSWORD_EMAIL = "@account/SEND_LOST_PASSWORD_EMAIL";
export const SEND_LOST_PASSWORD_EMAIL_REQUEST = "@account/SEND_LOST_PASSWORD_EMAIL_REQUEST";
export const SEND_LOST_PASSWORD_EMAIL_PENDING = "@account/SEND_LOST_PASSWORD_EMAIL_PENDING";
export const SEND_LOST_PASSWORD_EMAIL_FULFILLED = "@account/SEND_LOST_PASSWORD_EMAIL_FULFILLED";
export const SEND_LOST_PASSWORD_EMAIL_REJECTED = "@account/SEND_LOST_PASSWORD_EMAIL_REJECTED";

export const RESET_LOST_PASSWORD = "@account/RESET_LOST_PASSWORD";
export const RESET_LOST_PASSWORD_PENDING = "@account/RESET_LOST_PASSWORD_PENDING";
export const RESET_LOST_PASSWORD_FULFILLED = "@account/RESET_LOST_PASSWORD_FULFILLED";
export const RESET_LOST_PASSWORD_REJECTED = "@account/RESET_LOST_PASSWORD_REJECTED";

export const FETCH_USER_PAYMENT_DETAILS_REQUEST = "@account/FETCH_USER_PAYMENT_DETAILS_REQUEST";
export const FETCH_USER_PAYMENT_DETAILS_SUCCESS = "@account/FETCH_USER_PAYMENT_DETAILS_SUCCESS";

export const SEND_LOST_PASSWORD_FORM = 'sendLostPasswordForm'

export const UPDATE_PAYMENT_DATA_REJECTED = "@account/UPDATE_PAYMENT_DATA_REJECTED";

export const SAGA_FETCH_USER_REQUEST = "@account/SAGA_FETCH_USER_REQUEST"
export const SAGA_FETCH_USER_SUCCESS = "@account/SAGA_FETCH_USER_SUCCESS"
export const SAGA_FETCH_USER_FAILURE = "@account/SAGA_FETCH_USER_FAILURE"

export const SAGA_UPDATE_USER_REQUEST = "@account/SAGA_UPDATE_USER_REQUEST"
export const SAGA_UPDATE_USER_SUCCESS = "@account/SAGA_UPDATE_USER_SUCCESS"
export const SAGA_UPDATE_USER_FAILURE = "@account/SAGA_UPDATE_USER_FAILURE"

export const fetchPaymentDetailsByUserRequest = () => ({
    type: FETCH_USER_PAYMENT_DETAILS_REQUEST
});

export const fetchPaymentDetailsByUserSuccess = (payload) => ({
    type: FETCH_USER_PAYMENT_DETAILS_SUCCESS,  payload
});

export const setUpdatePaymentDetailsAction = (payload) => ({
    type: UPDATE_PAYMENT_DATA_REJECTED
})

export const fetchPaymentDetailsByUserThunk = () =>  async (dispatch, getState, {user: userClient})  => {
    const {entityId} = await getState().accountReducer.user;
    const {access_token} = await getState().loginReducer.auth;

    dispatch(fetchPaymentDetailsByUserRequest());
    if(entityId && access_token) {
        const {data} = await userClient.getPaymentDetails(access_token, entityId);
        dispatch(fetchPaymentDetailsByUserSuccess(data));
    }
}

export const resetAccountReducer = function () {
    return {
        type: RESET_ACCOUNT_REDUCER
    };
};

export const getUserThunk = () => {
    return (dispatch, getState, {user: userClient}) => {
        const accessToken = getState().loginReducer.auth.access_token;
        dispatch({
            type: GET_USER,
            payload: userClient.getUser(accessToken)
        });
    };
};

export const updateUserThunk = (userData) =>
    (dispatch, getState, {user: userClient}) =>
        new Promise((resolve, reject) => {
            const state = getState();
            const accessToken = state.loginReducer.auth.access_token;

            // todo: Since the BE is very susceptible to unrecognised fields, I suggest we validate the payload here somehow

            const promise = userClient.updateUser(accessToken, userData);

            dispatch({
                type: UPDATE_USER,
                payload: promise
            });

            promise
                .then((r) => resolve(r))
                .catch((e) => reject(e));
    });

export const resetUpdateUserFetched = () => {
    return {
        type: RESET_USER_UPDATE
    };
};

export const changePassword = (currentPassword, password) => {
    return (dispatch, getState, {user: userClient}) => {
        const accessToken = getState().loginReducer.auth.access_token;
        const login = getState().loginReducer.username;
        const promise = userClient.changePassword(accessToken, login, password);

        dispatch({
            type: CHANGE_PASSWORD,
            payload: promise
        });

        return promise;
    };
};

export const sendLostPasswordEmail = {type: SEND_LOST_PASSWORD_EMAIL_REQUEST}

export const resetLostPassword = (token, password) => {
    return (dispatch, getState, {user: userClient}) => {
        dispatch({
            type: RESET_LOST_PASSWORD,
            payload: userClient.resetLostPassword(token, password)
        });
    };
};

export const sagaFetchUserRequest = () => {
    return {
        type: SAGA_FETCH_USER_REQUEST
    }
};

export const sagaUpdateUserRequest = (userData) => {
    return {
        type: SAGA_UPDATE_USER_REQUEST,
        userData
    }
};
