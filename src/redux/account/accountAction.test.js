import * as accountActions from './accountAction'
import sinon from 'sinon'
import server from '../../../server/index'

describe('accountActions', () => {

    describe('fetchPaymentDetailsByUserThunk', () => {

        let dispatchSpy;
        let getStateSpy;
        let getStateStub;
        let APIClients;
        const paymentDetailsStub = {
            config: null,
            data: server.paymentDetails
        }

        describe('Positive scenario', () => {

            const thunkFn = accountActions.fetchPaymentDetailsByUserThunk();
            let getPaymentDetailsStub;

            beforeEach(() => {
                dispatchSpy = sinon.spy();
                getStateSpy = sinon.spy();
                getStateStub = sinon.stub();
                getPaymentDetailsStub = sinon.stub();

                getStateStub.returns({
                    accountReducer: {
                        user: {
                            entityId: "entityId-test"
                        },
                        paymentDetails: null
                    },
                    loginReducer: {
                        auth: {
                            access_token: 'token-id'
                        }
                    }
                });

                APIClients =  {getPaymentDetails: getPaymentDetailsStub};
                getPaymentDetailsStub.returns(Promise.resolve(paymentDetailsStub));
            });

            it('should return payment details for one user', (done) => {

                thunkFn(dispatchSpy, getStateStub, {user: APIClients})
                    .then(() => {
                        expect(dispatchSpy.callCount).toBe(2);
                        expect(getStateSpy.callCount).toEqual(0);
                        expect(getPaymentDetailsStub.callCount).toEqual(1);

                        expect(dispatchSpy.firstCall.args[0].type).toEqual(accountActions.FETCH_USER_PAYMENT_DETAILS_REQUEST);
                        expect(dispatchSpy.secondCall.args[0].type).toEqual(accountActions.FETCH_USER_PAYMENT_DETAILS_SUCCESS);

                        expect(dispatchSpy.secondCall.args[0].payload).toEqual(paymentDetailsStub.data);

                        done();
                    });
            })

        });

    });
});

