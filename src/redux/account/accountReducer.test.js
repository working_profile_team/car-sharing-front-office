import accountReducer from "./accountReducer";
import {
    CHANGE_PASSWORD_FULFILLED,
    CHANGE_PASSWORD_PENDING,
    CHANGE_PASSWORD_REJECTED,
    GET_USER_FULFILLED,
    GET_USER_PENDING,
    GET_USER_REJECTED, RESET_LOST_PASSWORD_FULFILLED,
    RESET_LOST_PASSWORD_PENDING, RESET_LOST_PASSWORD_REJECTED,
    RESET_USER_UPDATE,
    SEND_LOST_PASSWORD_EMAIL_FULFILLED,
    SEND_LOST_PASSWORD_EMAIL_PENDING,
    SEND_LOST_PASSWORD_EMAIL_REJECTED,
    UPDATE_USER_FULFILLED,
    UPDATE_USER_PENDING,
    UPDATE_USER_REJECTED,
    FETCH_USER_PAYMENT_DETAILS_SUCCESS,
    FETCH_USER_PAYMENT_DETAILS_REQUEST
} from "./accountAction";

describe('accountReducer', () => {
    it('correctly handles GET_USER_PENDING actions', () => {
        const result = accountReducer(undefined, { type: GET_USER_PENDING });

        expect(result.fetching).toBe(true);
        expect(result.fetched).toBe(false);
        expect(result.error).toBe(null);
    });

    it('correctly handles GET_USER_FULFILLED actions', () => {
        const result = accountReducer(undefined, {
            type: GET_USER_FULFILLED,
            payload: 'userData'
        });

        expect(result.fetching).toBe(false);
        expect(result.fetched).toBe(true);
        expect(result.error).toBe(null);
        expect(result.user).toBe('userData');
    });

    it('correctly handles GET_USER_REJECTED actions', () => {
        const result = accountReducer(undefined, {
            type: GET_USER_REJECTED,
            payload: '"errorData"'
        });

        expect(result.fetching).toBe(false);
        expect(result.fetched).toBe(false);
        expect(result.error).toBe('errorData');
    });

    it('correctly handles UPDATE_USER_PENDING actions', () => {
        const result = accountReducer(undefined, {
            type: UPDATE_USER_PENDING,
        });

        expect(result.fetching).toBe(true);
        expect(result.fetched).toBe(false);
        expect(result.updateUserFetched).toBe(false);
        expect(result.error).toBe(null);
    });

    it('correctly handles UPDATE_USER_FULFILLED actions', () => {
        const result = accountReducer(undefined, {
            type: UPDATE_USER_FULFILLED,
            payload: 'userData'
        });

        expect(result.fetching).toBe(false);
        expect(result.fetched).toBe(true);
        expect(result.updateUserFetched).toBe(true);
        expect(result.error).toBe(null);
    });

    it('correctly handles UPDATE_USER_REJECTED actions', () => {
        const result = accountReducer(undefined, {
            type: UPDATE_USER_REJECTED,
            payload: '"errorData"'
        });

        expect(result.fetched).toBe(true); // todo: sure about this?
        expect(result.updateUserFetched).toBe(true);
        expect(result.error).toBe('errorData');
    });

    it('correctly handles RESET_USER_UPDATE actions', () => {
        const result = accountReducer(undefined, {
            type: RESET_USER_UPDATE,
        });

        expect(result.updateUserFetched).toBe(false);
    });

    it('correctly handles CHANGE_PASSWORD_PENDING actions', () => {
        const result = accountReducer(undefined, {
            type: CHANGE_PASSWORD_PENDING,
        });

        expect(result.fetching).toBe(true);
        expect(result.fetched).toBe(false);
        expect(result.error).toBe(null);
    });

    it('correctly handles CHANGE_PASSWORD_REJECTED actions', () => {
        const result = accountReducer(undefined, {
            type: CHANGE_PASSWORD_REJECTED,
            payload: '"errorData"'
        });

        expect(result.fetching).toBe(false);
        expect(result.fetched).toBe(false);
        expect(result.error).toBe('errorData');
    });

    it('correctly handles CHANGE_PASSWORD_FULFILLED actions', () => {
        const result = accountReducer(undefined, {
            type: CHANGE_PASSWORD_FULFILLED
        });

        expect(result.fetching).toBe(false);
        expect(result.fetched).toBe(true);
        expect(result.error).toBe(null);
    });

    it('correctly handles SEND_LOST_PASSWORD_EMAIL_PENDING actions', () => {
        const result = accountReducer(undefined, {
            type: SEND_LOST_PASSWORD_EMAIL_PENDING,
        });

        expect(result.fetching).toBe(true);
        expect(result.fetched).toBe(false);
        expect(result.error).toBe(null);
        expect(result.reply).toBe(null);
    });

    it('correctly handles SEND_LOST_PASSWORD_EMAIL_FULFILLED actions', () => {
        const result = accountReducer(undefined, {
            type: SEND_LOST_PASSWORD_EMAIL_FULFILLED,
            payload: '"reply"'
        });

        expect(result.fetching).toBe(false);
        expect(result.fetched).toBe(true);
        expect(result.error).toBe(null);
        expect(result.reply).toBe('reply');
    });

    it('correctly handles SEND_LOST_PASSWORD_EMAIL_REJECTED actions', () => {
        const result = accountReducer(undefined, {
            type: SEND_LOST_PASSWORD_EMAIL_REJECTED,
            payload: 'reply'
        });

        expect(result.fetching).toBe(false);
        expect(result.fetched).toBe(true);
        expect(result.reply).toBe(null);
        expect(result.error).toBe('reply');
    });

    it('correctly handles RESET_LOST_PASSWORD_PENDING actions', () => {
        const result = accountReducer(undefined, {
            type: RESET_LOST_PASSWORD_PENDING,
        });

        expect(result.fetching).toBe(true);
        expect(result.fetched).toBe(false);
        expect(result.reply).toBe(null);
        expect(result.error).toBe(null);
    });

    it('correctly handles RESET_LOST_PASSWORD_FULFILLED actions', () => {
        const result = accountReducer(undefined, {
            type: RESET_LOST_PASSWORD_FULFILLED,
            payload: '"reply"'
        });

        expect(result.fetching).toBe(false);
        expect(result.fetched).toBe(true);
        expect(result.reply).toBe('reply');
    });

    it('correctly handles RESET_LOST_PASSWORD_REJECTED actions', () => {
        const result = accountReducer(undefined, {
            type: RESET_LOST_PASSWORD_REJECTED,
            payload: '"reply"'
        });

        expect(result.fetching).toBe(false);
        expect(result.fetched).toBe(true);
        expect(result.error).toBe('reply');
        expect(result.reply).toBe(null);
    });

    it('correctly handles FETCH_USER_PAYMENT_DETAILS_SUCCESS actions', () => {
        const result = accountReducer(undefined, {
            type: FETCH_USER_PAYMENT_DETAILS_SUCCESS,
            payload: 'payload'
        });

        expect(result.fetching).toBe(false);
        expect(result.error).toBe(null);
        expect(result.fetched).toBe(true);
        expect(result.reply).toBe(null);
        expect(result.paymentDetails).toBe('payload');
    });

    it('correctly handles FETCH_USER_PAYMENT_DETAILS_REQUEST actions', () => {
        const result = accountReducer(undefined, {
            type: FETCH_USER_PAYMENT_DETAILS_REQUEST,
            payload: 'payload'
        });

        expect(result.fetching).toBe(true);
        expect(result.error).toBe(null);
        expect(result.fetched).toBe(false);
        expect(result.reply).toBe(null);
        expect(result.paymentDetails).toBe(null);
    });
});
