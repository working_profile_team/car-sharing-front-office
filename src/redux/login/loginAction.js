export const SET_LOGIN_DETAILS = "@login/SET_LOGIN_DETAILS";

export const LOGOUT = "@login/LOGOUT";
export const SET_REDIRECT_ROUTE = "@login/SET_REDIRECT_ROUTE";
export const SET_USER_TOKEN = "@login/SET_USER_TOKEN";
export const RESET_LOGIN_REDUCER = "@login/RESET_LOGIN_REDUCER";

export const TOKEN_REQUEST = '@login/TOKEN_REQUEST'
export const TOKEN_REFRESH_REQUEST = '@login/TOKEN_REFRESH_REQUEST'

export const FETCH_TOKEN_PENDING = "@login/FETCH_TOKEN_PENDING";
export const FETCH_TOKEN_FULFILLED = "@login/FETCH_TOKEN_FULFILLED";
export const FETCH_TOKEN_REJECTED = "@login/FETCH_TOKEN_REJECTED";

export const REFETCH_TOKEN_PENDING = "@login/REFETCH_TOKEN_PENDING";
export const REFETCH_TOKEN_FULFILLED = "@login/REFETCH_TOKEN_FULFILLED";
export const REFETCH_TOKEN_REJECTED = "@login/REFETCH_TOKEN_REJECTED";
export const LOGIN_FORM_SUBMIT = "@login/FORM_SUBMIT"
export const RF_LOGIN = 'loginForm'


export const setLoginDetails = (fieldName, fieldValue) => {
    return {type: SET_LOGIN_DETAILS, payload: {fieldName, fieldValue}}
};

export const loginFormSubmit = (data) => {
    return {type: LOGIN_FORM_SUBMIT, payload:data}
}

export const setRedirectRoute = (redirectRoute) => {
    return {type: SET_REDIRECT_ROUTE, redirectRoute}
};

export const setUserToken = (userToken) => {
    return {type: SET_USER_TOKEN, userToken}
};

export const fetchTokenRequest = {type: TOKEN_REQUEST};

export const resetLoginReducer = () => {
    return {type: RESET_LOGIN_REDUCER}
};

export const logout = () => {
    return {type: LOGOUT}
};