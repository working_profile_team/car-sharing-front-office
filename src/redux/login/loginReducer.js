import {
    SET_LOGIN_DETAILS,
    FETCH_TOKEN_PENDING,
    FETCH_TOKEN_FULFILLED,
    FETCH_TOKEN_REJECTED,

    LOGOUT,
    SET_REDIRECT_ROUTE,
    SET_USER_TOKEN,
    RESET_LOGIN_REDUCER,

    REFETCH_TOKEN_PENDING,
    REFETCH_TOKEN_FULFILLED,
    REFETCH_TOKEN_REJECTED,
} from './loginAction.js'


var initState = {
    username: '',
    password: '',
    fetching: false,
    fetched: false,
    auth: {},
    error: null,
    last_fetch: undefined,
    //redirectRoute: '/find_cars',
    redirectRoute: '/account',
};

var loginReducer = (state = initState, action) => {

    switch (action.type) {
        case SET_LOGIN_DETAILS:
            return Object.assign({}, state, {
                [action.payload.fieldName]: action.payload.fieldValue
            });

        case FETCH_TOKEN_PENDING:
            return Object.assign({}, state, {
                fetching: true,
                auth: {},
                error: null,
                last_fetch: undefined,
                fetched: false,
            });
        case FETCH_TOKEN_REJECTED:
            return Object.assign({}, state, {
                fetching: false,
                error: action.payload,
                auth: {},
            });
        case FETCH_TOKEN_FULFILLED:
            return Object.assign({}, state, {
                fetching: false,
                fetched: true,
                auth: action.payload,
                last_fetch: Math.floor(Date.now() / 1000),
                error: null
            });
        case LOGOUT:
            return Object.assign({}, initState);
        case SET_REDIRECT_ROUTE:
            return Object.assign({}, state, { redirectRoute: action.redirectRoute });
        case SET_USER_TOKEN:
            return Object.assign({}, state, { auth: action.userToken });
        case RESET_LOGIN_REDUCER:
            return Object.assign({}, initState);
        case REFETCH_TOKEN_PENDING:
            return Object.assign({}, state, {
                fetching: true,
                error: null,
                fetched: false,
            });
        case REFETCH_TOKEN_REJECTED:
            return Object.assign({}, state, {
                fetching: false,
                error: JSON.parse(action.payload),
                auth: {},
            });
        case REFETCH_TOKEN_FULFILLED:
            return Object.assign({}, state, {
                fetching: false,
                fetched: true,
                auth: action.payload,
                last_fetch: Math.floor(Date.now() / 1000),
                error: null
            });
        default:
            return state;
    }
}

export default loginReducer;
