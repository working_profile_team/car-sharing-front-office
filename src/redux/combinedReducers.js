import { combineReducers } from "redux";
import loginReducer from './login/loginReducer';
import registerReducer from './registration/registrationReducer';
import accountReducer from "./account/accountReducer";
import languageReducer from "./language/languageReducer";
import vehicleReducer from "./vehicle/vehicleReducer";
import guiReducer from "./gui/guiReducer";
import themeReducer from "./theme/themeReducer"
import billingReducer from "./billing/reducer"
import promoReducer from './promocodes/promoCodesReducer'
import { reducer as rfFormReducer } from 'redux-form'
export default combineReducers(
    {
        loginReducer,
        registerReducer,
        accountReducer,
        languageReducer,
        vehicleReducer,
        guiReducer,
        themeReducer,
        billingReducer,
        promoReducer,
        form: rfFormReducer
    });