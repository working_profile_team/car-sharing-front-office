import React, {Component} from 'react';
import {connect} from "react-redux";

import {logout} from "../redux/login/loginAction";
import {resetMap} from "../redux/vehicle/vehicleAction";
import {changeTheme} from "../redux/theme/themeAction";
import BigScreenNav from "../components/Navigation/BigScreenNav";
import SmallScreenNav from "../components/Navigation/SmallScreenNav";
import {getFlagSrc} from "../selectors/language";

class Nav extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            openPopover: false,
            name: this.props.language.localizedStrings["Login"],
            mode: '' // bigscreen vs smallscreen
        };

        this.mediaQueryList = null;
    }

    /*Menu for languages*/
    showPopover = (event) => {
        event.preventDefault();

        this.setState({
            openPopover: true,
            anchorEl: event.currentTarget,
        });
    };

    hidePopover = () => {
        this.setState({
            openPopover: false,
        });
    };

    handleLogout = () => {
        this.props.logout();
        this.props.resetMap();
    };

    handleResize = (e) => {
        this.setState({mode: e.matches ? 'bigscreen' : 'smallscreen'});
    };

    componentDidMount() {
        if (window.matchMedia) {
            this.mediaQueryList = window.matchMedia("(min-width: 768px)");
            this.mediaQueryList.addListener(this.handleResize);
            this.handleResize(this.mediaQueryList);
        } else {
            this.setState({mode: 'bigscreen'});
        }
    }

    componentWillUnmount() {
        if (this.mediaQueryList && this.mediaQueryList.removeListener) {
            this.mediaQueryList.removeListener(this.handleResize);
        }
    }

    render() {
        if (this.state.mode === '') {
            return <div/>;
        }
        const NavComponent = this.state.mode === 'bigscreen' ? BigScreenNav : SmallScreenNav;

        return <NavComponent
            name={this.props.name}
            route={this.props.route}
            language={this.props.language}
            theme={this.props.theme}
            muiTheme={this.props.muiTheme}
            showPopover={this.showPopover}
            hidePopover={this.hidePopover}
            languageSelectorOpen={this.state.openPopover}
            languageSelectorEl={this.state.anchorEl}
            handleLogout={this.handleLogout}
            isUserAuthenticated={this.props.auth && this.props.auth.access_token}
            isUserConnected = {this.props.auth.access_token!==undefined}
            flagSrc={this.props.flagSrc}
        />;
    };
}

const mapStateToProps = (state) => {
    return {
        auth: state.loginReducer.auth,
        name: state.guiReducer.name,
        route: state.guiReducer.route,
        user: state.accountReducer.user,
        flagSrc: getFlagSrc(state),
    }
};



const mapDispatchToProps = (dispatch, props) => {
    return {
        logout: () => {
            dispatch(logout())
        },
        resetMap: () => {
            dispatch(resetMap())
        },
        changeTheme: (theme)=>{
            dispatch(changeTheme(theme))
        }
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Nav);
