import React, { Component } from 'react';
import {
    Route,
    Redirect
} from 'react-router-dom';
import { connect } from "react-redux";
import {bindActionCreators} from "redux";
import {sagaFetchUserRequest} from "../redux/account/accountAction";
import {setRedirectRoute} from "../redux/login/loginAction.js";

class PrivateRoute extends Component {

    show = (routeProps, otherProps = {}) => {

        if (this.props.auth === undefined || this.props.auth.access_token === undefined) {
            this.props.setRedirectRoute(this.props.path);
            return <Redirect to={{
                pathname: '/login',
                state: { from: routeProps.location }
            }} />
        } else {
            let Comp = this.props.component;
            if(!this.props.user.entityId){
               this.props.fetchUser();
            }
            return (<Comp routeProps {...otherProps} />);
        }
    }

    render() {
        let nProps = Object.assign({}, this.props);
        delete nProps['component'];
        delete nProps['auth'];
        delete nProps['otherProps'];
        return (
            <div>
                <Route  {...nProps} render={(routeProps) => {
                    return this.show(routeProps, this.props.otherProps)
                    }
                } />
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    auth: state.loginReducer.auth,
    userToken: state.loginReducer.userToken,
    user: state.accountReducer.user
})

const mapDispatchToProps = dispatch => ({
    fetchUser: bindActionCreators(sagaFetchUserRequest, dispatch),
    setRedirectRoute: bindActionCreators(setRedirectRoute, dispatch)
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PrivateRoute);
