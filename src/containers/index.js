import React, { Component } from 'react';
import classnames from 'classnames';
import './containers.css'

import {
    Route,
    Switch,
    withRouter
} from 'react-router-dom';
import { MuiThemeProvider } from 'material-ui/styles';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import Nav from './Nav';
import Contact from './Contact';
import Account from './Account';
import Registration from './Registration/Registration';
import Faq from './Faq';
import Login from './Route/Login';
import FindCars from './FindCars';
import Billing from './Billing';
import FreeMinutes from './FreeMinutes';
import PrivateRoute from './PrivateRoute';
import SendLostPasswordEmail from '../components/users/account/SendLostPasswordEmail'
import ResetLostPassword from '../components/users/account/ResetLostPassword'

import { setUserToken, resetLoginReducer } from '../redux/login/loginAction.js'
import { resetRegistrationReducer } from "../redux/registration/registrationAction.js"
import { changeName, setApp } from "../redux/gui/guiAction.js"
import { getParameterByName } from "../redux/utils"
import { changeTheme } from "../redux/theme/themeAction";
import localize from 'components/Localize'
import {

    ROUTE_BILLING, ROUTE_CONTACT, ROUTE_FAQ, ROUTE_FIND_CARDS, ROUTE_FREE_MINUTES, ROUTE_LOGIN,
    ROUTE_MY_ACCOUNT, ROUTE_REGISTER
} from "../constants";

import { clientConf } from '../App.js'
import { setUpdatePaymentDetailsAction } from "../redux/account/accountAction";
import { setlanguage, } from 'redux/language/languageAction';

//Some route is not exact after add webview mode, recheck

const isRanFromNativeApp = (url) => {
    return getParameterByName('apps', url);
}

class Root extends Component {
    static defaultProps = {
        theme: {},
        muiTheme: {}
    };

    shouldRedirect = false;

    componentWillMount() {
        const apps = isRanFromNativeApp(this.props.location.search);

        let langParam = getParameterByName('lang', this.props.location.search) ? getParameterByName('lang', this.props.location.search) : localize.getCurrent();
        let userToken = getParameterByName('UserToken', this.props.location.search);

        if (apps === "true") {
            localize.setLanguage(langParam);
            this.props.setLanguage(langParam);
            this.props.setUserToken({ access_token: userToken });
            this.props.setApp(true);
        }
        else {
            this.props.setApp(false);
        }

        // this handles the scenario when we come from adyen payment
        // and it would be better to refresh the page, so that all the query params
        // that adyen sends us, are cleared from the browser's URL
        let adyenAuthParam = getParameterByName('authResult');
        if (adyenAuthParam && adyenAuthParam === 'AUTHORISED') {
            let { protocol = null, host = null, pathname = null } = window.location;
            if (protocol && host && pathname && pathname.indexOf('register') === -1) {
                const mobileQueryParams = apps === "true" ? `?apps=true&lang=${langParam}&UserToken=${userToken}` : '';
                window.location = `${protocol}//${host}${pathname}${mobileQueryParams}`;
            }
        }

        if (adyenAuthParam && adyenAuthParam === 'REFUSED') {
            this.props.setUpdatePaymentDetailsResult(false);
        }
    }

    componentWillUpdate(nextProps) {
        const oldPathname = this.props.location.pathname;
        const newPathname = nextProps.location.pathname;
        const splittedOld = oldPathname.split('/');
        //We don't want to trigger the redirection if you are already in a route that has subroute. So we split the subroute and we check if the next route contains it.
        //We don't need to do the check if the subroute is an empty string or undefined
        if (nextProps.location !== this.props.location && ((!splittedOld[1]) || newPathname.indexOf(splittedOld[1]) === -1)) {
            this.shouldRedirect = true;
        }
    }

    /*Change in "amp" to have the amp style*/
    componentDidUpdate() {
        const { location, changeName, language } = this.props;

        switch (true) {
            case location.pathname === "/find_cars":
                changeName(language.localizedStrings["findCar"], ROUTE_FIND_CARDS);
                break;
            case location.pathname === "/faq":
                changeName("FAQ", ROUTE_FAQ);
                break;
            case location.pathname === "/contact":
                changeName(language.localizedStrings["Contact"], ROUTE_CONTACT);
                break;
            case location.pathname === "/login":
                changeName(language.localizedStrings["Login"], ROUTE_LOGIN);
                break;
            case location.pathname === "/billing":
                changeName(language.localizedStrings["billing"], ROUTE_BILLING);
                break;
            case location.pathname === "/account":
                changeName(language.localizedStrings["account"], ROUTE_MY_ACCOUNT);
                break;
            case location.pathname === "/free_minutes":
                changeName(language.localizedStrings["free_minutes"], ROUTE_FREE_MINUTES);
                break;
            case location.pathname.indexOf("register") !== -1:
                changeName(language.localizedStrings["register"], ROUTE_REGISTER);
                break;
            default:
                changeName(language.localizedStrings["Login"], ROUTE_LOGIN);
                break;
        }
    }

    redirectTo(path) {
        window.location = path;

        return null;
    }

    render() {
        const PrivateRouteWrap = withRouter(PrivateRoute);
        const { adyenApiConf, adyenConf, baseAppUrl, language, muiTheme, theme, fleetId, accountReducer } = this.props;

        const otherProps = {
            baseAppUrl,
            language,
            muiTheme,
            theme,
            fleetId,
            accountReducer
        };

        const adyenProps = {
            adyenConf, adyenApiConf
        };

        const pathNav = clientConf.navigationBarPath || {};
        let nav;
        if (!this.props.app) {
            nav = <Nav
                muiTheme={this.props.muiTheme}
                theme={this.props.theme}
                language={this.props.language}
            />;
        }
        const constainerClassNames = classnames({
            "container": true,
            "container--apps": isRanFromNativeApp(this.props.location.search)
        })


        const shouldRedirect = this.shouldRedirect;
        const redirectTo = this.redirectTo;


        return (
            <MuiThemeProvider muiTheme={this.props.muiTheme}>
                <div className='root'>
                    {nav}
                    <div className={constainerClassNames}>
                        {/* <Route exact path="/" component={FindCars} /> */}
                        <Switch>
                            <Route exact path="/" render={(props) => <Login {...props} {...otherProps} />} />
                            {
                                <Route exact path={'/contact'}
                                    render={(props) =>
                                        pathNav.contact && shouldRedirect ? redirectTo(pathNav.contact) :
                                            <Contact {...props} {...otherProps} />
                                    }
                                />
                            }
                            {
                                <Route exact path={'/account'}
                                    render={(props) =>
                                        pathNav.account && shouldRedirect ? redirectTo(pathNav.account) :
                                            <PrivateRouteWrap exact path={'/account'} component={Account}
                                                otherProps={{
                                                    ...props,
                                                    ...otherProps,
                                                    ...adyenProps
                                                }} />
                                    }
                                />
                            }
                            {
                                <Route exact path={'/login'}
                                    render={(props) =>
                                        pathNav.login && shouldRedirect ? redirectTo(pathNav.login) :
                                            <Login {...props} {...otherProps} />
                                    }
                                />
                            }
                            {
                                <Route path={'/register'}
                                    render={(props) =>
                                        pathNav.register && shouldRedirect ? redirectTo(pathNav.register) :
                                            <Registration {...props} {...otherProps} {...adyenProps} shouldRedirect={shouldRedirect} />
                                    }
                                />
                            }
                            {
                                <Route exact path={'/faq'}
                                    render={(props) =>
                                        pathNav.faq && shouldRedirect ? redirectTo(pathNav.faq) :
                                            <Faq {...props} {...otherProps} />
                                    }
                                />
                            }
                            {
                                <Route exact path={'/find_cars'}
                                    render={(props) =>
                                        pathNav.findCar && shouldRedirect ? redirectTo(pathNav.findCar) :
                                            <FindCars {...props} {...otherProps} />
                                    }
                                />
                            }
                            {
                                <Route exact path={'/billing'}
                                    render={(props) =>
                                        pathNav.billing && shouldRedirect ? redirectTo(pathNav.billing) :
                                            <PrivateRouteWrap exact path={'/billing'} component={Billing}
                                                {...props} otherProps={otherProps} />
                                    }
                                />
                            }
                            {
                                <Route exact path={'/free_minutes'}
                                    render={(props) =>
                                        pathNav.free_minutes && shouldRedirect ? redirectTo(pathNav.free_minutes) :
                                            <PrivateRouteWrap exact path={'/free_minutes'} component={FreeMinutes}
                                                otherProps={otherProps} />
                                    }
                                />
                            }
                            {
                                <Route exact path={'/send_lost_password_email'}
                                    render={(props) =>
                                        pathNav.send_lost_password_email && shouldRedirect ? redirectTo(pathNav.send_lost_password_email) :
                                            <SendLostPasswordEmail {...props} {...otherProps} />
                                    }
                                />
                            }
                            {
                                <Route exact path={'/reset_lost_password'}
                                    render={(props) =>
                                        pathNav.reset_lost_password && shouldRedirect ? redirectTo(pathNav.reset_lost_password) :
                                            <ResetLostPassword {...props} {...otherProps} />
                                    }
                                />
                            }
                            <Route component={NoMatch} />
                        </Switch>
                    </div>
                </div>
            </MuiThemeProvider>
        );
    }

}


const mapStateToProps = (state, ownProps) => {
    return {
        languageReducer: state.languageReducer,
        registerReducer: state.registerReducer,
        loginReducer: state.loginReducer,
        app: state.guiReducer.app,
        muiTheme: ownProps.muiTheme,
        theme: ownProps.theme,
        isConnected: state.loginReducer.auth.access_token,
        accountReducer: state.accountReducer
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        setLanguage: bindActionCreators(setlanguage, dispatch),
        setUserToken: bindActionCreators(setUserToken, dispatch),
        resetLoginReducer: bindActionCreators(resetLoginReducer, dispatch),
        resetRegistrationReducer: bindActionCreators(resetRegistrationReducer, dispatch),
        changeName: bindActionCreators(changeName, dispatch),
        setApp: bindActionCreators(setApp, dispatch),
        changeTheme: bindActionCreators(changeTheme, dispatch),
        setUpdatePaymentDetailsResult: bindActionCreators(setUpdatePaymentDetailsAction, dispatch)
    }
};

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(Root));

const NoMatch = ({ location }) => {
    window.location = `/?origin=${location.pathname}`
};