import React, {Component} from 'react';
import { Snackbar }  from 'material-ui';
import { connect } from "react-redux";
import bindActionCreators from "redux/es/bindActionCreators";
import { OnCloseMessage } from "../../redux/gui/guiAction";

class SystemMessage extends Component {
    handleRequestClose = () => {
        this.props.close();
    };

    render() {
        return (
            <Snackbar
                open={this.props.open}
                message={this.props.data.text ? this.props.language.localizedStrings[this.props.data.text] : ''}
                action="close"
                autoHideDuration={4000}
                onRequestClose={this.handleRequestClose}
            />
        )
    }
}
const mapStateToProps = (state) => ({
    open: state.guiReducer.systemMessageOpen,
    data: state.guiReducer.systemMessageData,
});

const mapDispatchToProps = dispatch => {
    return {
        close: bindActionCreators(OnCloseMessage, dispatch)
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SystemMessage);
