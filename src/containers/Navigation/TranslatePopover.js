import React, { Component } from 'react';
import MenuItem from 'material-ui/MenuItem';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import { LANGUAGE_ENGLISH, LANGUAGE_FRENCH, LANGUAGE_SPANISH, LANGUAGE_SWEDISH } from "../../constants";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { setlanguage } from "../../redux/language/languageAction";
import { sagaUpdateUserRequest } from "../../redux/account/accountAction";
import { sagaFetchUserRequest } from "../../redux/account/accountAction";
import localize from 'components/Localize'

const clientName = process.env.REACT_APP_CLIENT_NAME || 'vulog';
const apiConf = require('../../resources/api/' + clientName + '.json');
const clientConf = apiConf

const firstLang = clientConf.forceLang;
const onlyOneLang = clientConf.onlyOneLang;


class TranslatePopover extends Component {
    onSelectLanguage = (language) => {
        localize.setLanguage(language);
        this.props.setLanguage(language);
        if (this.props.user.entityId) {
            this.props.updateUser({ locale: language })
        }
        this.props.onRequestClose();
    };

    renderFrenchOption = () => {
        return <MenuItem
            id="Selenium-French"
            onClick={() => this.onSelectLanguage(LANGUAGE_FRENCH)}>
            <img className="flag" src='/images/frenchFlag.png' alt="French" />
            Français
        </MenuItem>
    };

    renderSpanishOption = () => {
        return <MenuItem
            id="Selenium-Spanish"
            onClick={() => this.onSelectLanguage(LANGUAGE_SPANISH)}>
            <img className="flag" src='/images/spanishFlag.png' alt="Spanish" />
            español
        </MenuItem>
    };

    renderSwedishOption = () => {
        return <MenuItem
            id="Selenium-Spanish"
            onClick={() => this.onSelectLanguage(LANGUAGE_SWEDISH)}>
            <img className="flag" src='/images/swedishFlag.png' alt="Swedish" />
            Svenska
        </MenuItem>
    };

    renderEnglishOption = () => {
        return <MenuItem
            id="Selenium-English"
            onClick={() => this.onSelectLanguage(LANGUAGE_ENGLISH)}>
            <img className="flag" src='/images/englishFlag.jpg' alt="English" />
            English
        </MenuItem>
    };

    render() {
        const { onRequestClose, open, anchorEl } = this.props;
        return (

            <Popover
                open={open}
                anchorEl={anchorEl}
                anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
                targetOrigin={{ horizontal: 'left', vertical: 'top' }}
                onRequestClose={onRequestClose}
            >


                {
                    !onlyOneLang && <Menu style={{ overflowX: "hidden" }}>
                        {firstLang === LANGUAGE_FRENCH && this.renderFrenchOption()}
                        {firstLang === LANGUAGE_SPANISH && this.renderSpanishOption()}
                        {firstLang === LANGUAGE_SWEDISH && this.renderSwedishOption()}
                        {this.renderEnglishOption()}
                    </Menu>
                }

                {
                    onlyOneLang && <Menu style={{ overflowX: "hidden" }}>
                        {firstLang === LANGUAGE_FRENCH && this.renderFrenchOption()}
                        {firstLang === LANGUAGE_SPANISH && this.renderSpanishOption()}
                        {firstLang === LANGUAGE_ENGLISH && this.renderEnglishOption()}
                        {firstLang === LANGUAGE_SWEDISH && this.renderSwedishOption()}
                    </Menu>
                }

            </Popover>
        )
    }
}


const mapDispatchToProps = dispatch => ({
    setLanguage: bindActionCreators(setlanguage, dispatch),
    updateUser: bindActionCreators(sagaUpdateUserRequest, dispatch),
    fetchUser: bindActionCreators(sagaFetchUserRequest, dispatch),
});

const mapStateToProps = (state) => ({
    user: state.accountReducer.user
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TranslatePopover);
