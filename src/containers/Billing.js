import React, { Component } from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import Main from "../components/Billing/Main";
import {clientConf} from "../App"
import {
    downloadInvoiceRequest,
    fetchMyTripsRequest
} from "../redux/billing/actions";

import {userSelector} from 'selectors/account'

export class Billing extends Component {

    componentWillMount(){
        this.props.fetchMyTripsRequest();
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevProps.userSelector.email !== this.props.userSelector.email) {
            this.props.fetchMyTripsRequest();
        }
    }

    handleListTrips = (data) => {
        const trips = [];
        Object.keys(data).forEach((tripKey) => {
            trips.push(data[tripKey])
        });
        return trips;
    };

    render() {
        const trips = this.handleListTrips(this.props.billing.data);

        const {
            billing: { isFetching },
            downloadInvoiceRequest,
            language,
        } = this.props;

        return (
            <Main isFetching={isFetching}
                  downloadInvoiceRequest={downloadInvoiceRequest}
                  language={language}
                  trips={trips}
                  urlInvoice={this.props.urlInvoice}
                  isAppMode={this.props.isAppMode}>
            </Main>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    downloadInvoiceRequest: bindActionCreators(downloadInvoiceRequest, dispatch),
    fetchMyTripsRequest: bindActionCreators(fetchMyTripsRequest, dispatch)
});

const mapStateToProps = (state) => ({
    billing: state.billingReducer,
    userSelector: userSelector(state),
    urlInvoice: (invoiceId) => `${clientConf.baseEndpoint}invoices/${invoiceId}/pdf?access_token=${state.loginReducer.auth.access_token}&x-api-key=${clientConf.authenticatedAccess.apiKey}`,
    isAppMode: !!state.guiReducer && !!state.guiReducer.app,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Billing);