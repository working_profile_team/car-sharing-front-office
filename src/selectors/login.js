export const accessTokenSelector = state => state.loginReducer.auth.access_token;
export const loginSelector = state => state.loginReducer;