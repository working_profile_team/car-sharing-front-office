import { LANGUAGE_ENGLISH, LANGUAGE_FRENCH, LANGUAGE_SPANISH, LANGUAGE_SWEDISH } from "../constants";

export const languageSelector = state => state.languageReducer;

export const getFlagSrc = state => {
    switch (state.languageReducer.language) {
        case LANGUAGE_FRENCH:
            return '/images/frenchFlag.png';

        case LANGUAGE_SPANISH:
            return '/images/spanishFlag.png';

        case LANGUAGE_SWEDISH:
            return '/images/swedishFlag.png';

        case LANGUAGE_ENGLISH:
            return '/images/englishFlag.jpg';

        default:
            return '/images/englishFlag.jpg';
    }
};