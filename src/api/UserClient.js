import axios from 'axios';
import qs from 'qs';
import moment from 'moment'
import {buildReply} from './utils';
import ApiClient from "./ApiClient";
import jwt from 'jsonwebtoken';
import mocked from './../resources/api/vulogMock.json'


const UserClientDateFormat = 'YYYY-MM-DD'
export default class UserClient extends ApiClient {
    constructor(apiConf) {
        super();
        this.apiConf = apiConf;
    }

    fetchToken = (username, password) => {
        if (this.apiConf.useMockApi){
            const apiUrl = `http://localhost:${this.mockApiPort}/fetchToken`;
            return this.get(apiUrl);
        }
        const apiUrl = this.apiConf.authEndpoint;
        const jsonData = qs.stringify({
            username,
            password,
            client_id: this.apiConf.authenticatedAccess.clientId,
            securityOptions: 'SSL_OP_NO_SSLv3',
            client_secret: this.apiConf.authenticatedAccess.clientSecret,
            grant_type: 'password'
        });
        return this.postWithUrlEncoded(apiUrl, jsonData);
    };

    refreshToken = (refreshToken) => {
        if (this.apiConf.useMockApi){
            const apiUrl = `http://localhost:${this.mockApiPort}/fetchToken`;
            return this.get(apiUrl);
        }
        const apiUrl = this.apiConf.authEndpoint;
        const jsonData = qs.stringify({
            refresh_token: refreshToken,
            client_id: this.apiConf.authenticatedAccess.clientId,
            securityOptions: 'SSL_OP_NO_SSLv3',
            client_secret: this.apiConf.authenticatedAccess.clientSecret,
            grant_type: 'refresh_token'
        });
        return this.postWithUrlEncoded(apiUrl, jsonData);
    };

    fetchAnonymousToken = () => {
        if (this.apiConf.useMockApi){
            const apiUrl = `http://localhost:${this.mockApiPort}/fetchToken`;
            return this.get(apiUrl);
        }
        const apiUrl = this.apiConf.authEndpoint;
        const jsonData = qs.stringify({
            client_id: this.apiConf.anonymousAccess.clientId,
            securityOptions: 'SSL_OP_NO_SSLv3',
            client_secret: this.apiConf.anonymousAccess.clientSecret,
            grant_type: 'client_credentials'
        });
        return this.postWithUrlEncoded(apiUrl, jsonData);
    };

    fetchSignUpToken = () => {
        if (this.apiConf.useMockApi){
            const apiUrl = `http://localhost:${this.mockApiPort}/fetchToken`;
            return this.get(apiUrl);
        }
        const apiUrl = this.apiConf.authEndpoint;
        const jsonData = qs.stringify({
            client_id: this.apiConf.signUp.clientId,
            securityOptions: 'SSL_OP_NO_SSLv3',
            client_secret: this.apiConf.signUp.clientSecret,
            grant_type: 'client_credentials'
        });
        return this.postWithUrlEncoded(apiUrl, jsonData);
    };

    getUser = (accessToken) => {
        if (this.apiConf.useMockApi) {
            const apiUrl = `http://localhost:${this.mockApiPort}/user`;
            return this.get(apiUrl);
        }
        const apiUrl = `${this.apiConf.baseEndpoint}user`;
        return this.get(apiUrl, accessToken);
    };

    updateUser = (accessToken, user) => {
        if (this.apiConf.useMockApi){
            user = {...mocked.user, ...user};
            user.id = jwt.decode(accessToken).sub;
            const apiUrl = `http://localhost:${this.mockApiPort}/user`;
            return this.put(apiUrl, user);
        }
        const apiUrl = `${this.apiConf.baseEndpoint}user`;
        return this.put(apiUrl, user, accessToken);
    }

    fetchMyTrips = (accessToken, startDate = moment().subtract(10, 'years').format(UserClientDateFormat), endDate = moment().format(UserClientDateFormat)) => {
        if (this.apiConf.useMockApi) {
            const apiUrl = `http://localhost:${this.mockApiPort}/fetchMyTrips`;
            return this.get(apiUrl);
        }
        const apiUrl = `${this.apiConf.baseEndpoint}invoices/tripDetails?startDate=${startDate}&endDate=${endDate}`;
        return this.get(apiUrl, accessToken);
    };

    urlInvoice = (token, invoiceId) => `${this.apiConf.baseEndpoint}invoices/${invoiceId}/pdf?access_token=${token}&x-api-key=${this.apiConf.authenticatedAccess.apiKey}`;
}
