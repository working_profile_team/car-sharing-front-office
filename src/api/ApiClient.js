import { buildReply } from './utils';
import axios from 'axios';

export default class ApiClient {
    constructor(apiConf) {
        this.apiConf = apiConf;
        this.mockApiPort = process.env.REACT_APP_FORCE_MOCK_API_PORT;
    }

    postWithUrlEncoded = (apiUrl, payload) => {
        return new Promise((resolve, reject) => {
            const requestConfig = this._getRequestConfig("post", apiUrl, reject, payload, { urlEncoded: true });
            this._callAPIRequest(requestConfig, resolve, reject);
        });
    };

    get = (apiUrl, accessToken) => {
        return new Promise((resolve, reject) => {
            const requestConfig = this._getRequestConfig("get", apiUrl, reject, null, { accessToken });
            this._callAPIRequest(requestConfig, resolve, reject);
        });
    };


    put = (apiUrl, payload, accessToken) => {
        return new Promise((resolve, reject) => {
            const requestConfig = this._getRequestConfig("put", apiUrl, reject, payload, { accessToken });
            this._callAPIRequest(requestConfig, resolve, reject);
        });
    };

    _getRequestConfig = (requestMethod, apiUrl, reject, payload, headersConfig) => {
        if (this.apiConf.useMockApi) {
            if (!apiUrl.includes(`http://localhost:${this.mockApiPort}`)) {
                reject(`The URL ${apiUrl} is not a mocked URL in http://localhost:${this.mockApiPort} domain`);
            }
        }
        let headers = {
            'Cache-Control': 'no-cache',
            'Content-Type': 'application/json',
            'X-API-Key': this.apiConf.authenticatedAccess.apiKey
        };
        if (headersConfig.accessToken) {
            headers.authorization = 'Bearer ' + headersConfig.accessToken;
        }
        if (headersConfig.urlEncoded) {
            headers['Content-Type'] = 'application/x-www-form-urlencoded';
        }
        const requestConfig = {
            url: apiUrl,
            method: requestMethod,
            timeout: this.apiConf["timeout"],
            headers
        };

        if (payload) {
            requestConfig.data = payload;
        }

        return requestConfig;
    }

    _callAPIRequest = (requestConfig, resolve, reject) => {
        axios.request(requestConfig)
            .then((response) => {
                resolve(response.data);
            })
            .catch(this._handleError(reject));
    }

    _handleError = (reject) => {
        return (error) => {
            if (error.response) {
                if (!error.response.status === 401) {

                    reject(buildReply(error.response.status, 'Server error'));
                } else {
                    reject({ "code": "401", "message": "Unauthorised" });
                }
            } else if (error.request) {
                reject(buildReply(500, 'No response received from the server'));
            }
            else {
                reject(buildReply(500, `Unable to make HTTP request: ${error.message}`));
            }
        };
    }
}
