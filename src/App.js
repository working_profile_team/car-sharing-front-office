import React, { Component } from 'react';

import deepmerge from 'deepmerge';

import injectTapEventPlugin from 'react-tap-event-plugin';
import { getMuiTheme } from 'material-ui/styles';

import localize from './components/Localize'

import {
  BrowserRouter,
} from 'react-router-dom';
import { Provider } from "react-redux";

import Routes from './containers';
import './App.css'

import configureStore from './redux';

const clientName = process.env.REACT_APP_CLIENT_NAME || 'vulog';

const themeData = require('./resources/themes/' + clientName + '.json');
const apiConf = require('./resources/api/' + clientName + '.json');
export const clientConf = apiConf
const adyenConf = require('./resources/adyen/' + clientName + '.json');
const adyenApiConf = require('./resources/api/adyen.json');

const {fleetId} = apiConf;

const store = configureStore(apiConf);

const muiTheme = getMuiTheme(deepmerge( themeData.theme, {
    cardTitle: {padding: '30px', textAlign: 'center'},
    cardText: {padding: '0 30px 30px 30px'},
    raisedButton: {
        fontSize: 20,
        height: 400,
        disabledColor: themeData.theme.palette.disabledColor
    }
}));


const language = {list: localize.languageList, localizedStrings: localize.localizedStrings};

class App extends Component {
  render() {
    const baseAppUrl = typeof global.window !== 'undefined' ? window.location.origin : '';

    return (
      <Provider store={store}>
          <BrowserRouter>
            <Routes
                language={language}
                muiTheme={muiTheme}
                theme={themeData}
                adyenConf={adyenConf}
                adyenApiConf={adyenApiConf}
                baseAppUrl={baseAppUrl}
                fleetId={fleetId}
            />
          </BrowserRouter>
      </Provider>
    );
  }
}



injectTapEventPlugin();
/**
 * Create-react-app main component
 * @module App
 */

export default App;
