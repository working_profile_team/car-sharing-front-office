import FileSaver from 'file-saver';

export const filterOutEmptyProperties = (obj) =>
    Object.keys(obj)
        .filter(fieldName => obj[fieldName] !== '' && obj[fieldName] !== null && obj[fieldName] !== undefined)
        .reduce((acc, fieldName) => {
            acc[fieldName] = obj[fieldName];
            return acc;
        }, {});

export const downloadBlobPDFData = (data, filename) => {
    const blob = new Blob([data]);
    filename = `${filename || 'file'}.pdf`;
    FileSaver.saveAs(blob, filename);
};