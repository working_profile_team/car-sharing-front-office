function isUsername(username){
    if (typeof username !== 'string') return 'Username cannot be empty'
    if (username.length < 4) return 'Username is too short'
    if (username.length > 255) return 'Username is too long'
}

export default isUsername