function isPassword(password) {
    if (typeof password !== 'string') return 'Password cannot be empty'
    if (password.length < 4) return 'Password is too short'
    if (password.length > 20) return 'Password is too long'
    if (!password.match(/[a-z]/i)) return 'Password should contain at least one letter'
    if (!password.match(/[0-9]/i)) return 'Password should contain at least one number'
}

export default isPassword