import isPassword from '../isPassword'

describe('Password validator', () => {
  it('rejects a short password', () => {
      expect(typeof isPassword('sh0')).toBe('string')
  })

    it('rejects a long password', () => {
        expect(typeof isPassword('1longlonglonglonglonglonglonglonglonglonglong')).toBe('string')
    })

    it('rejects a password without letters', () => {
        expect(typeof isPassword('12345678')).toBe('string')
    })

    it('rejects a password without numbers', () => {
        expect(typeof isPassword('lettersABC')).toBe('string')
    })

    it('accepts a valid password', () => {
        expect(typeof isPassword('Pa55wordABC')).toBe('undefined')
    })
})