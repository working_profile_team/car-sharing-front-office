import isPhoneNumber from '../isPhoneNumber'

describe('isPhoneNumber validator', () => {
    it('rejects a boolean', () => {
        expect(typeof isPhoneNumber(false)).toBe('string')
    })

    it('rejects a string container letters', () => {
        expect(typeof isPhoneNumber('069343ABC')).toBe('string')
    })

    it('rejects a shorter number', () => {
        expect(typeof isPhoneNumber('1234')).toBe('string')
    })

    it('rejects a longer number', () => {
        expect(typeof isPhoneNumber('123456789012345')).toBe('string')
    })

    it('accepts a 10 character long number', () => {
        expect(typeof isPhoneNumber('0123456789')).toBe('undefined')
    })
})