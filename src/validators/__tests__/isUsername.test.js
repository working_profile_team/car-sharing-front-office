import isUsername from '../isUsername'

describe('isUsername validator', ()=>{
    it('rejects an empty username', ()=>{
        expect(typeof isUsername(false)).toBe('string')
        expect(typeof isUsername(true)).toBe('string')
        expect(typeof isUsername('')).toBe('string')
    })

    it('rejects a short username', ()=>{
        expect(typeof isUsername('usr')).toBe('string')
    })

    it('rejects a long username', ()=>{
        expect(typeof isUsername('qwertyuiopasdfghjklzxcvbnmdsjkalfjaslkdjfalskqwertyuiopasdfghjklzxcvbnmdsjkalfjaslkdjfalskqwertyuiopasdfghjklzxcvbnmdsjkalfjaslkdjfalskqwertyuiopasdfghjklzxcvbnmdsjkalfjaslkdjfalskqwertyuiopasdfghjklzxcvbnmdsjkalfjaslkdjfalskqwertyuiopasdfghjklzxcvbnmdsjkalfjaslkdjfalskqwertyuiopasdfghjklzxcvbnmdsjkalfjaslkdjfalskqwertyuiopasdfghjklzxcvbnmdsjkalfjaslkdjfalskqwertyuiopasdfghjklzxcvbnmdsjkalfjaslkdjfalsk')).toBe('string')
    })

    it('accepts a valid username', ()=>{
        expect(typeof isUsername('JulienBrun')).toBe('undefined')
    })
})