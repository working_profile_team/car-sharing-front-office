import isNotEmpty from '../isNotEmpty'

describe('isNotEmpty validator', () => {
    it('declines an empty string', () => {
        expect(typeof isNotEmpty('')).toBe('string')
    })

    it('accepts a filled string', () => {
        expect(typeof isNotEmpty('abc')).toBe('undefined')
    })

    it('accepts a number', () => {
        expect(typeof isNotEmpty(3432)).toBe('undefined')
    })

    it('accepts a truthy value', () => {
        expect(typeof isNotEmpty(true)).toBe('undefined')
    })

    it('accepts a falsy value', () => {
        expect(typeof isNotEmpty(true)).toBe('undefined')
    })
})