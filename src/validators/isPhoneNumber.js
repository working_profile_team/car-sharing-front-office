function isPhoneNumber(number){
    if (!number) return 'Needs to be number not string'
    if (number.match(/[a-z]/i)) return 'Needs to be number not string'
    if (number.length < 10) return 'Number is too short'
    if (number.length > 10) return 'Number is too long'
}

export default isPhoneNumber