import React, { Component } from 'react';
import { connect } from "react-redux";
import { RaisedButton } from 'material-ui';
import PersonForm from '../../Forms/PersonForm';
import HeaderCard from '../../Header';
import { List, ListItem } from 'material-ui/List';
import localize from 'components/Localize'
import Languages from "constants/Languages";
import _ from "lodash";


class Person extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: 'show'
        }
    }

    handleCard = () => {
        this.setState({
            status: this.state.status === 'edit' ? 'show' : 'edit'
        })
    }

    updatePerson = (person) => {
        this.props.updateUser(person);
    }

    componentWillUpdate(newProps) {
        if (this.props.updateUserFetched === false && newProps.updateUserFetched === true) {
            this.setState({status: 'show'});
        }
    }

    render() {


        const {localizedStrings: t} = this.props.language;
        const userLanguage = _.find(Languages(localize.localizedStrings.global.languages), (l) => (l.code === this.props.user.locale))
        const userLanguageName = userLanguage ? userLanguage.name : '';
        const show = (
            <div>
                <HeaderCard
                    title={this.props.language.localizedStrings["YourDetails"]}
                >
                    <RaisedButton
                        id="Selenium-buttonEditPerson"
                        label={this.props.language.localizedStrings["Edit"]}
                        className="pull-right"
                        onTouchTap={this.handleCard}
                    />
                </HeaderCard>
                <List>
                    <ListItem className="Selenium-AccountDetailsListItem" disabled={true}
                              primaryText={this.props.user.firstName}
                              secondaryText={this.props.language.localizedStrings["FirstName"]}/>
                    <ListItem className="Selenium-AccountDetailsListItem" disabled={true}
                              primaryText={this.props.user.middleName}
                              secondaryText={this.props.language.localizedStrings["MiddleName"]}/>
                    <ListItem className="Selenium-AccountDetailsListItem" disabled={true}
                              primaryText={this.props.user.lastName}
                              secondaryText={this.props.language.localizedStrings["LastName"]}/>
                    <ListItem className="Selenium-AccountDetailsListItem" disabled={true}
                              primaryText={this.props.user.phoneNumber}
                              secondaryText={this.props.language.localizedStrings["PhoneNumber"]}/>
                    <ListItem className="Selenium-AccountDetailsListItem" disabled={true} primaryText={userLanguageName}
                              secondaryText={localize.localizedStrings.global.languages.language}/>
                </List>
            </div>
        );

        const edit = (
            <div>
                <HeaderCard
                    title={this.props.language.localizedStrings["YourDetails"]}
                >
                    <RaisedButton
                        id="Selenium-buttonClosePerson"
                        label={this.props.language.localizedStrings["Close"]}
                        className="pull-right"
                        onTouchTap={this.handleCard}
                    />
                </HeaderCard>
                <PersonForm
                    language={this.props.language}
                    mode='myaccount'
                    firstName={this.props.user.firstName}
                    middleName={this.props.user.middleName}
                    lastName={this.props.user.lastName}
                    gender={this.props.user.gender}
                    phoneNumber={this.props.user.phoneNumber}
                    locale={this.props.user.locale}
                    handleSubmit={this.updatePerson}
                    submitButtonLabel={t.actions.common.save}
                />
            </div>
        );

        return (
            <div id="Selenium-AccountScene-PersonalDetailsSection">
                {this.state.status === 'edit' ? edit : show}
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        user: state.accountReducer.user,
        updateUserFetched: state.accountReducer.updateUserFetched,
    }
};

export default connect(
    mapStateToProps
)(Person);
