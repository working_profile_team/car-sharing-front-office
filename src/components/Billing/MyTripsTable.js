import React, { Component } from 'react';
import FlatButton from 'material-ui/FlatButton';
import { Row, Col } from 'react-flexbox-grid';
import { Card } from 'material-ui';
import moment from 'moment'
import { clientConf } from "../../App";

import Timeline from 'material-ui-icons/Timeline'
import CreditCard from 'material-ui-icons/CreditCard'
import DirectionsCar from 'material-ui-icons/DirectionsCar'
import Timelapse from 'material-ui-icons/Timelapse'
import QueryBuilder from 'material-ui-icons/QueryBuilder'
import muiThemeable from 'material-ui/styles/muiThemeable';
import './Billing.css';

const formatInvoiceNumber = (number) => {return parseFloat(Math.round(number * 100) / 100).toFixed(2)};

class MyTripsTable extends Component {

    styles = theme => ({
        button: {
            backgroundColor: theme.primary1Color,
            color: theme.alternateTextColor,
            width: '100%',
            marginTop: '15px'
        },
        label: {
            color: theme.primary1Color,
            verticalAlign: 'middle'
        },
        itemLast: {
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            lineHeight: '21px',
            marginBottom: 'inherit'
        },
        item: {
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            lineHeight: '21px',
            marginBottom: '21px'
        },
        notTrip: {
            textAlign: 'center',
            fontSize: '20px'
        },
        largeIcon: {
            width: 60,
            height: 60,
        },
    });

    _handleDownloadInvoice = (invoiceId) => {
        this.props.downloadInvoiceRequest(invoiceId);
    };

    render() {
        const handleDownloadInvoice = this._handleDownloadInvoice;
        const sortedTrips = this.props.trips.sort((trip1, trip2) => {
            return moment(trip2.trip.tripStartDate).isSameOrBefore(moment(trip1.trip.tripStartDate)) ? -1 : 1;
        });

        const theme = this.props.muiTheme.palette;
        const styles = this.styles(theme);
        const text = this.props.language.localizedStrings;
        const isAppMode = this.props.isAppMode;

        return (
            <div>
                <Row>
                    {sortedTrips.length > 0 ?

                        sortedTrips.map((trip, tripIndex) => {
                            return (
                                <Col key={tripIndex}
                                     xs={12} sm={6} md={6} lg={4}>
                                    <Card className="tripsCard">
                                        <Row style={styles.item}>
                                            <QueryBuilder className="icons" color={theme.primary1Color} /> &nbsp;
                                            <span id={`Selenium-MyTripsTable-started-${tripIndex}`}
                                                style={styles.label}>
                                                {text.billingPage.started}: &nbsp;
                                                </span>
                                            {moment(trip.trip.tripStartDate).format(clientConf.displayDateTimeFormat)}
                                        </Row>

                                        <Row style={styles.item}>
                                            <Timelapse className="icons" color={theme.primary1Color} /> &nbsp;
                                            <span id={`Selenium-MyTripsTable-duration-${tripIndex}`}
                                                style={styles.label}>
                                                {text.billingPage.duration}: &nbsp;
                                                </span>
                                            {moment.duration(trip.trip.drivingDuration, "minutes").humanize()}
                                        </Row>

                                        <Row style={styles.item}>
                                            <CreditCard className="icons" color={theme.primary1Color} /> &nbsp;
                                            <span id={`Selenium-MyTripsTable-charged-${tripIndex}`}
                                                style={styles.label}>
                                                {text.billingPage.charged}: &nbsp;
                                                </span>
                                            {formatInvoiceNumber(trip.totalWithTax)} {trip.invoice.currency}
                                        </Row>

                                        <Row style={styles.item}>
                                            <DirectionsCar className="icons" color={theme.primary1Color} /> &nbsp;
                                            <span id={`Selenium-MyTripsTable-vehicle-${tripIndex}`}
                                                style={styles.label}>
                                                {text.billingPage.vehicle}: &nbsp;
                                                </span>
                                            {trip.vehiculeModelName} - {trip.vehiculePlate}
                                        </Row>

                                        <Row style={styles.item}>
                                            <Timeline className="icons" color={theme.primary1Color} /> &nbsp;
                                            <span id={`Selenium-MyTripsTable-distance-${tripIndex}`}
                                                style={styles.label}>
                                                {text.billingPage.distance}: &nbsp;
                                                </span>
                                             {formatInvoiceNumber(trip.trip.distance)}{trip.distanceUnit}
                                        </Row>

                                        <Row style={styles.itemLast}>
                                            {!isAppMode && <FlatButton id={`Selenium-MyTripsTable-download-${tripIndex}`}
                                                label={text.billingPage.downloadInvoice}
                                                style={styles.button}
                                                onClick={() => handleDownloadInvoice(trip.invoice.id)}
                                            />}
                                            {isAppMode &&
                                            <FlatButton
                                                label={text.billingPage.downloadInvoice}
                                                style={styles.button}
                                                primary href={`newtab:${this.props.urlInvoice(trip.invoice.id)}`}
                                             />}
                                        </Row>
                                    </Card>
                                </Col>
                            );
                        })
                        :
                        <Col xs={12} sm={12} md={12} lg={12}>
                            <div style={styles.notTrip}>
                                <p>{text.billingPage.notTrip}</p>
                                <br />
                                <DirectionsCar style={styles.largeIcon} color={theme.primary1Color} />
                            </div>
                        </Col>
                    }
                </Row>
            </div>
        );
    }
}

export default muiThemeable()(MyTripsTable);
