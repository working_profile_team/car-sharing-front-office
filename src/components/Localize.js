import LocalizedStrings from 'react-localization';
import { loadState, saveState } from "redux/store/localStorage";

import EN from './../redux/language/localization/en';
import FR from './../redux/language/localization/fr';
import ES from './../redux/language/localization/es';
import SV from './../redux/language/localization/sv';
import { LANGUAGE_ENGLISH, LANGUAGE_FRENCH, LANGUAGE_SPANISH, LANGUAGE_SWEDISH } from "./../constants";



/*
    Simple singleton to support localize content
*/
class Localize {

    constructor() {
        const clientName = process.env.REACT_APP_CLIENT_NAME || 'vulog';
        const clientConf = require('resources/api/' + clientName + '.json');
        const Localization = {
            [LANGUAGE_ENGLISH]: EN,
            [LANGUAGE_FRENCH]: FR,
            [LANGUAGE_SPANISH]: ES,
            [LANGUAGE_SWEDISH]: SV,
        };
        const languageList = {
            [LANGUAGE_ENGLISH]: "English",
            [LANGUAGE_FRENCH]: "Français",
            [LANGUAGE_SPANISH]: "Spanish",
            [LANGUAGE_SWEDISH]: "Swedish",
        };
        const language = loadState('language');
        this.language = language ? language : clientConf.forceLang;
        this.languageList = languageList;
        this.localizedStrings = new LocalizedStrings(Localization);
        this.setLanguage(this.language);
    }


    setLanguage = (lg) => {
        this.language = lg;
        saveState("language", this.language);
        this.localizedStrings.setLanguage(this.language);
    }

    getCurrent = () => {
        return this.localizedStrings.getLanguage();
    }

    getLanguages = () => {
        return this.languageList;
    }

}

const instance = new Localize();

export default instance;
